from enum import Enum

from django.db import models


class Genre(Enum):
    ACTION = 'Action'
    COMEDY = 'Comedy'
    HORROR = 'Horror'
    SCIFI = 'Sci-Fi'


class Movie(models.Model):
    title = models.CharField(max_length=23)
    genre = models.CharField(max_length=21, choices=[(tag.name, tag.value) for tag in Genre])
    poster = models.URLField(max_length=451)
    imdb_score = models.DecimalField(max_digits=2, decimal_places=1)
    cast = models.ManyToManyField('Actor', blank=True)


class Actor(models.Model):
    name = models.CharField(max_length=101)
    filmography = models.ManyToManyField(Movie, blank=True)
    birthday = models.DateField()
