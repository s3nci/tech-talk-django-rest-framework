from django.contrib import admin

from movies.models import Movie, Actor


class MovieAdmin(admin.ModelAdmin):
    pass


class ActorAdmin(admin.ModelAdmin):
    pass


admin.site.register(Movie, MovieAdmin)
admin.site.register(Actor, ActorAdmin)
