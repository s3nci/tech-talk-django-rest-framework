# Quickstart

## Install python and pip

```bash
# MacOS (brew)
brew install python

# Debian-Based
apt install python3

# Arch-Based
python -S python

# Windows
Do a barrelroll...
(well, u download an installer and execute it...)
```

## Install django & django-rest-framework

```bash
pip3 install django django-rest-framework django-rest-swagger
```

### Pro-Tip

For better dependency manangement (especially if you have multiple python apps running on the same machine) you want to consider creating and using a python environment for each project.
I usually default to [pipenv](https://github.com/pypa/pipenv), but there are various options.

## Create new Project

```bash
djano-admin startproject my_project
```

## Write Models

```python
# models.py
class Genre(Enum):
    ACTION = 'Action'
    COMEDY = 'Comedy'
    HORROR = 'Horror'
    SCIFI = 'Sci-Fi'


class Movie(models.Model):
    title = models.CharField(max_length=23)
    genre = models.CharField(max_length=21, choices=[(tag, tag.value) for tag in Genre])
    poster = models.URLField(max_length=451)
    imdb_score = models.DecimalField(max_digits=1, decimal_places=1)
    cast = models.ManyToManyField('Actor')


class Actor(models.Model):
    name = models.CharField(max_length=101)
    filmography = models.ManyToManyField(Movie)
    birthday = models.DateField()
```

## Generate Migrations

```bash
python3 manage.py makemigrations
```

## Run Migrations

```bash
python3 manage.py migrate
```

## Add rest-framework to INSTALLED_APPS

```python
# settings.py
INSTALLED_APPS = [
    'rest_framework',
]
```

## Write Serializers

```python
# serializers.py
class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = '__all__'


class ActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Actor
        fields = '__all__'
```

## Write Views

```python
# views.py
class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class ActorViewSet(viewsets.ModelViewSet):
    queryset = Actor.objects.all()
    serializer_class = ActorSerializer
```

## Add views to router & register router

```python
# urls.py
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'movies', views.MovieViewSet)
router.register(r'actors', views.ActorViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
```

## Run Server

```bash
python3 manage.py runserver
```

## Optional Steps

### Extra Swag (free OpenAPI-Spec)

```python
# urls.py
urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='movies')),
    url(r'^swagger$', get_swagger_view(title="Movies API")),
]
```

```python
# settings.py
INSTALLED_APPS = [
    'rest_framework_swagger',
]
```

### Admin Interface

```python
# urls.py
urlpatterns = [
    path('admin/', admin.site.urls),
]
```

# Resources

* [Django Project](https://www.djangoproject.com/)
* [Django Rest Framework (DRF)](https://www.django-rest-framework.org/)
* [Django REST Swagger](https://django-rest-swagger.readthedocs.io/en/latest/)
